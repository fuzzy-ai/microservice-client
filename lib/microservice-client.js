// microservice-client.coffee
// useful superclass for microservice clients
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const util = require('util')
const EventEmitter = require('events')
const assert = require('assert')

const _ = require('lodash')
const async = require('async')
const web = require('@fuzzy-ai/web')
const debug = require('debug')('microservice-client')
const LRU = require('lru-cache')

const CacheItem = require('./cache-item')

// For retries, timeslot in seconds

const TIME_SLOT = 0.1

// For truncated exponential backoff, this is where we truncate. Max wait = 30m,
// So max combined wait is 1h

const MAX_BACKOFF = 15

class TimeoutError extends Error {
  constructor (method, url, wait, max) {
    super(`${method} on ${url} took ${wait} seconds, max ${max}`)
    this.method = method
    this.url = url
    this.wait = wait
    this.max = max
    this.name = 'TimeoutError'
  }
}

class ContentTypeError extends Error {
  constructor (method, url, contentType, content) {
    super(`${method} on ${url} returned ${contentType}: ${content.substr(0, 16)}`)
    this.method = method
    this.url = url
    this.contentType = contentType
    this.content = content
    this.name = 'ContentTypeError'
  }
}

const isJSONType = function (type) {
  const expected = 'application/json'
  return type.substr(0, expected.length) === expected
}

const DEFAULTS = {
  queueLength: 16,
  maxWait: Infinity,
  cacheSize: 100,
  timeout: 1000
}

const ARGS_FIELDS = _.concat(_.keys(DEFAULTS), ['root', 'key', 'webClient'])

class MicroserviceClient extends EventEmitter {
  constructor (...args) {
    super()
    debug('Before processing defaults')

    debug(this)

    _.assign(this, DEFAULTS)

    debug('After processing defaults, before processing args')

    debug(this)

    if ((args.length === 1) && _.isObject(args[0])) {
      _.assign(this, _.pick(args[0], ARGS_FIELDS))
    } else if (args.length < 1) {
      throw new Error('Constructor requires one argument: root')
    } else {
      /* eslint-disable prefer-destructuring */
      this.root = args[0]
      if (args.length >= 2) {
        this.key = args[1]
      }
      if (args.length >= 3) {
        this.queueLength = args[2]
      }
      if (args.length >= 4) {
        this.maxWait = args[3]
      }
      if (args.length >= 5) {
        this.cacheSize = args[4]
      }
      if (args.length >= 6) {
        this.timeout = args[5]
      }
    }
    /* eslint-enable prefer-destructuring */

    debug('After processing args')

    debug(this)

    assert(_.isString(this.root), 'Root URL must be a string')
    assert(_.isFinite(this.queueLength) && (this.queueLength > 0),
      'Queue length must be a finite number greater than zero')
    assert((_.isFinite(this.maxWait) && (this.maxWait > 0)) || (this.maxWait === Infinity),
      'Max wait must be a finite number greater than zero, or Infinity')
    assert(_.isFinite(this.cacheSize) && (this.cacheSize > 0),
      'Cache size must be a number greater than zero')

    debug(`root = ${this.root}`)
    debug(`key = ${this.key}`)
    debug(`queueLength = ${this.queueLength}`)
    debug(`maxWait = ${this.maxWait}`)
    debug(`cacheSize = ${this.cacheSize}`)
    debug(`timeout = ${this.timeout}`)

    this.cache = new LRU(this.cacheSize)
    this.q = async.queue(this._request.bind(this), this.queueLength)

    if (this.webClient != null) {
      debug('Using passed-in WebClient')
    } else {
      debug('Creating WebClient')
      this.webClient = new web.WebClient({timeout: this.timeout})
    }
  }

  get (relative, callback) {
    assert(_.isString(relative), 'relative URL must be a string')
    assert(_.isFunction(callback), 'callback must be a function')

    debug(`get('${relative}')`)

    const task = {
      method: 'GET',
      relative,
      start: Date.now()
    }
    return this.q.push(task, callback)
  }

  post (relative, reqBody, callback) {
    assert(_.isString(relative), 'relative URL must be a string')
    assert((reqBody != null), 'POST request must have a body')
    assert(_.isFunction(callback), 'callback must be a function')

    debug(`post('${relative}')`)
    if (!_.isString(reqBody)) {
      reqBody = JSON.stringify(reqBody)
    }
    const task = {
      method: 'POST',
      relative,
      reqBody,
      start: Date.now()
    }
    return this.q.push(task, callback)
  }

  put (relative, reqBody, callback) {
    assert(_.isString(relative), 'relative URL must be a string')
    assert((reqBody != null), 'PUT request must have a body')
    assert(_.isFunction(callback), 'callback must be a function')

    debug(`put('${relative}')`)

    if (!_.isString(reqBody)) {
      reqBody = JSON.stringify(reqBody)
    }

    const task = {
      method: 'PUT',
      relative,
      reqBody,
      start: Date.now()
    }

    return this.q.push(task, callback)
  }

  patch (relative, reqBody, callback) {
    assert(_.isString(relative), 'relative URL must be a string')
    assert((reqBody != null), 'PATCH request must have a body')
    assert(_.isFunction(callback), 'callback must be a function')

    debug(`put('${relative}')`)

    if (!_.isString(reqBody)) {
      reqBody = JSON.stringify(reqBody)
    }

    const task = {
      method: 'PATCH',
      relative,
      reqBody,
      start: Date.now()
    }

    return this.q.push(task, callback)
  }

  delete (relative, callback) {
    assert(_.isString(relative), 'relative URL must be a string')
    assert(_.isFunction(callback), 'callback must be a function')

    debug(`delete('${relative}')`)

    const task = {
      method: 'DELETE',
      relative,
      start: Date.now()
    }

    return this.q.push(task, callback)
  }

  // Force this to be async

  stop (callback) {
    return setImmediate(() => {
      this.webClient.stop()
      return callback(null)
    })
  }

  _joinUrl (root, relative) {
    assert(_.isString(root), 'root URL must be a string')
    assert(_.isString(relative), 'relative URL must be a string')

    if ((root[root.length - 1] === '/') && (relative[0] === '/')) {
      return root + relative.slice(1)
    } else {
      return root + relative
    }
  }

  // Exponential backoff algorithm

  _waitTime (retries) {
    const c = retries > MAX_BACKOFF ? MAX_BACKOFF : retries
    return _.random(0, Math.pow(2, c - 1)) * TIME_SLOT
  }

  _request (task, callback) {
    let ci, lm
    debug('requesting task')
    debug(util.inspect(task))
    assert(_.isObject(task), 'Task must be an object')
    const {method, relative, reqBody, retries} = task
    assert(_.isString(method), 'Method must be a string')
    assert(_.isString(relative), 'Relative URL must be a string')
    assert((reqBody == null) || _.isString(reqBody),
      'Request body must be a string or null')
    assert((retries == null) || _.isFinite(retries),
      'If defined, retries must be a number')
    const headers = {
      'Content-Type': 'application/json; charset=utf-8'
    }
    if (_.isString(this.key)) {
      headers['Authorization'] = `Bearer ${this.key}`
    }

    const full = this._joinUrl(this.root, relative)

    // Cache lookup. We only do this for GET requests, for now.

    if (method === 'GET') {
      debug(`Looking for ${full} in cache`)
      ci = this.cache.get(full)
      if ((ci == null)) {
        this._note('cache-miss', full)
      } else {
        debug(`Found ${full} in cache`)
        debug(ci)
        this._note('cache-hit', full, ci)
        if ((ci.expires != null) && (ci.expires > Date.now())) {
          debug(`${full} not expired, so returning it exactly`)
          this._note('cache-results', full, ci)
          return callback(null, JSON.parse(ci.body))
        } else {
          if (ci.lastModified != null) {
            lm = (new Date(ci.lastModified)).toUTCString()
            headers['If-Modified-Since'] = lm
            debug(`If-Modified-Since = ${headers['If-Modified-Since']}`)
          }

          if (ci.etag) {
            headers['If-None-Match'] = ci.etag
            debug(`If-None-Match = ${headers['If-None-Match']}`)
          }
        }
      }
    }

    const args = {
      method,
      full,
      headers,
      reqBody
    }

    debug(util.inspect(args))

    return this.webClient.request(method, full, headers, reqBody, (err, res, resBody) => {
      if (!err) {
        debug(res.statusCode)
        debug(res.headers)
        if (res.statusCode === 304) {
          this._note('cache-unmodified', full, ci)
          return callback(null, JSON.parse(ci.body))
        } else {
          if (ci != null) {
            this._note('cache-modified', full, ci)
          }
          const ct = res.headers['content-type']
          if ((res.statusCode !== 204) && ((ct == null) || !isJSONType(ct))) {
            return callback(new ContentTypeError(method, full, ct, resBody))
          } else if (resBody && (resBody.length > 0)) {
            try {
              const results = JSON.parse(resBody)
              // We don't want to cache bad stuff
              if (method === 'GET') {
                debug(`New cache item for ${full}`)
                ci = new CacheItem(full, resBody)
                lm = res.headers['last-modified']
                const exp = res.headers['expires']
                const etag = res.headers['etag']
                if (lm != null) {
                  ci.lastModified = Date.parse(lm)
                  debug(`ci.lastModified = ${ci.lastModified}`)
                }
                if (exp != null) {
                  ci.expires = Date.parse(exp)
                  debug(`ci.expires = ${ci.expires}`)
                }
                if (etag != null) {
                  ci.etag = etag
                  debug(`ci.etag = ${ci.etag}`)
                }
                debug(`Adding ${full} cache item to cache`)
                this.cache.set(full, ci)
              }
              // So exceptions don't bubble back up
              return setImmediate(callback, null, results)
            } catch (error) {
              err = error
              this._noteError(err)
              return callback(err, null)
            }
          } else {
            return callback(null)
          }
        }
      } else {
        debug('Emitting error')
        // Emit an error, whether we retry or not
        this._noteError(err)
        debug(err)
        debug("Checking if it's a client error")
        if (err.name === 'ClientError') {
          debug('Client error; goes back to the caller')
          return callback(err, null)
        } else {
          debug('Server error; goes into retry queue')
          return this._retry(task, callback)
        }
      }
    })
  }

  _retry (task, callback) {
    debug('retrying task')
    debug(util.inspect(task))
    const actual = (Date.now() - task.start) / 1000
    if (actual > this.maxWait) {
      const full = this._joinUrl(this.root, task.relative)
      const err = new TimeoutError(task.method, full, actual, this.maxWait)
      debug(util.inspect(err))
      return callback(err)
    } else {
      if (task.retries != null) {
        task.retries++
      } else {
        task.retries = 1
      }
      let to = this._waitTime(task.retries)
      if ((Date.now() + (to * 1000)) > (task.start + (this.maxWait * 1000))) {
        to = Math.max(((task.start + (this.maxWait * 1000)) - Date.now()) / 1000, 0)
      }
      debug(`Waiting ${to} seconds`)
      const addToQueue = () => {
        return this.q.push(task, callback)
      }
      return setTimeout(addToQueue, to * 1000)
    }
  }

  _noteError (err) {
    return this._note('error', err)
  }

  _note (event, ...args) {
    return setImmediate(() => {
      if (this.listenerCount(event) > 0) {
        return this.emit(event, ...Array.from(args))
      }
    })
  }
}

module.exports = MicroserviceClient
