/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// client-error-test.coffee
// Test the handling of 4xx HTTP error codes
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const microserviceClientBatch = require('./batch')

process.on('uncaughtException', (err) => {
  console.error(err)
  return process.exit(-1)
})

vows
  .describe('ClientError')
  .addBatch(microserviceClientBatch({
    'and we GET a resource that is not there': {
      topic (client) {
        client.get('/does-not-exist', (err, body) => {
          if (err) {
            if (err.statusCode === 404) {
              return this.callback(null, err)
            } else {
              return this.callback(err, null)
            }
          } else {
            return this.callback(new Error('Unexpected success'))
          }
        })
        return undefined
      },
      'it works' (err, obj) {
        assert.ifError(err)
      },
      'its error is correct' (err, obj) {
        assert.ifError(err)
        assert.isObject(obj)
        assert.isString(obj.name)
        assert.equal(obj.name, 'ClientError')
        assert.isNumber(obj.statusCode)
        assert.equal(obj.statusCode, 404)
        assert.isObject(obj.headers)
        for (const name in obj.headers) {
          const value = obj.headers[name]
          assert.isString(name)
          assert.isString(value)
        }
        assert.isString(obj.url)
        assert.equal(obj.url, 'http://localhost:1516/does-not-exist')
        assert.isString(obj.verb)
        assert.equal(obj.verb, 'GET')
        assert.isString(obj.body)
        assert.isString(obj.message)
        assert.equal(obj.message, 'GET on http://localhost:1516/does-not-exist resulted in 404 Not Found')
      }
    }
  })).export(module)
