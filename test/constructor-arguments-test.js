/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// named-arguments-test.coffee
// Test using named instead of positional arguments

// Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const env = require('./env')

const ROOT = `http://${env.HOSTNAME}:${env.PORT}/`
const KEY = env.APP_KEY_UNITTEST

vows.describe('Named arguments to constructor')
  .addBatch({
    'When we create a client with named arguments for required arguments': {
      topic () {
        try {
          const MicroserviceClient = require('../lib/microservice-client')
          const client = new MicroserviceClient({
            root: ROOT,
            key: KEY
          })
          this.callback(null, client)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, client) {
        assert.ifError(err)
      },
      'required properties look correct' (err, client) {
        assert.ifError(err)
        assert.equal(client.root, ROOT)
        assert.equal(client.key, KEY)
      },
      'default required properties look correct' (err, client) {
        assert.ifError(err)
        assert.equal(client.queueLength, 16)
        assert.equal(client.maxWait, Infinity)
        assert.equal(client.cacheSize, 100)
        assert.equal(client.timeout, 1000)
      }
    }
  })
  .addBatch({
    'When we create a client with named arguments for optional arguments': {
      topic () {
        try {
          const MicroserviceClient = require('../lib/microservice-client')
          const client = new MicroserviceClient({
            root: ROOT,
            key: KEY,
            queueLength: 8,
            maxWait: 60,
            cacheSize: 1000,
            timeout: 60000
          })
          this.callback(null, client)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, client) {
        assert.ifError(err)
      },
      'required properties look correct' (err, client) {
        assert.ifError(err)
        assert.equal(client.root, ROOT)
        assert.equal(client.key, KEY)
      },
      'optional required properties look correct' (err, client) {
        assert.ifError(err)
        assert.equal(client.queueLength, 8)
        assert.equal(client.maxWait, 60)
        assert.equal(client.cacheSize, 1000)
        assert.equal(client.timeout, 60000)
      }
    }})
  .addBatch({
    'When we create a client with positional arguments for required arguments': {
      topic () {
        try {
          const MicroserviceClient = require('../lib/microservice-client')
          const client = new MicroserviceClient(ROOT, KEY)
          this.callback(null, client)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, client) {
        assert.ifError(err)
      },
      'required properties look correct' (err, client) {
        assert.ifError(err)
        assert.equal(client.root, ROOT)
        assert.equal(client.key, KEY)
      },
      'default required properties look correct' (err, client) {
        assert.ifError(err)
        assert.equal(client.queueLength, 16)
        assert.equal(client.maxWait, Infinity)
        assert.equal(client.cacheSize, 100)
        assert.equal(client.timeout, 1000)
      }
    }})
  .addBatch({
    'When we create a client with positional arguments for optional arguments': {
      topic () {
        try {
          const MicroserviceClient = require('../lib/microservice-client')
          const client = new MicroserviceClient(ROOT, KEY, 8, 60, 1000, 60000)
          this.callback(null, client)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, client) {
        assert.ifError(err)
      },
      'required properties look correct' (err, client) {
        assert.ifError(err)
        assert.equal(client.root, ROOT)
        assert.equal(client.key, KEY)
      },
      'optional required properties look correct' (err, client) {
        assert.ifError(err)
        assert.equal(client.queueLength, 8)
        assert.equal(client.maxWait, 60)
        assert.equal(client.cacheSize, 1000)
        assert.equal(client.timeout, 60000)
      }
    }})
  .addBatch({
    'When we create a client with named arguments missing root': {
      topic () {
        const callback = this.callback
        try {
          const MicroserviceClient = require('../lib/microservice-client')
          const client = new MicroserviceClient({key: KEY}) // eslint-disable-line no-unused-vars
          callback(new Error('Unexpected success'))
        } catch (err) {
          this.callback(null)
        }
        return undefined
      },
      'it fails correctly' (err) {
        assert.ifError(err)
      }
    }})
  .addBatch({'When we create a client with positional arguments missing root': {
    topic () {
      const callback = this.callback
      try {
        const MicroserviceClient = require('../lib/microservice-client')
        const client = new MicroserviceClient() // eslint-disable-line no-unused-vars
        callback(new Error('Unexpected success'))
      } catch (err) {
        this.callback(null)
      }
      return undefined
    },
    'it fails correctly' (err) {
      assert.ifError(err)
    }
  }}).export(module)
