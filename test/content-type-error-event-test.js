/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// content-type-error-event-test.coffee
// Test the get() method of microservice-client
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert
const debug = require('debug')('microservice-client:content-type-error-event-test')

const microserviceClientBatch = require('./batch')

const errorBatch = function (url, errorType) {
  const batch = {
    topic (client) {
      const { callback } = this
      client.get(url, (err, data) => {
        if (err && (err.name === errorType)) {
          debug(err.message)
          return callback(null)
        } else if (err) {
          return callback(err)
        } else {
          return callback(new Error('Unexpected success'))
        }
      })
      return undefined
    },
    'it fails correctly' (err) {
      assert.ifError(err)
    }
  }
  return batch
}

vows
  .describe('Test for specific error on non-JSON content type')
  .addBatch(microserviceClientBatch({
    'and we GET plain text': errorBatch('/plain-text', 'ContentTypeError'),
    'and we GET HTML5': errorBatch('/some-html5', 'ContentTypeError')
  })).export(module)
