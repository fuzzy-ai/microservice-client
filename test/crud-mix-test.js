/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// crud-mix-test.coffee
// Do a mix of CRUD activities on a test server
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert
const debug = require('debug')('microservice-client:crud-mix-test')
const _ = require('lodash')
const async = require('async')

const microserviceClientBatch = require('./batch')
const env = require('./env')

process.on('uncaughtException', (err) => {
  console.error(err)
  return process.exit(1)
})

vows.describe('CRUD mix')
  .addBatch(microserviceClientBatch({
    'and we run a mix of CRUD activities': {
      topic (client) {
        let widgets = [env.ENSURE_WIDGET.split(':')[0]]
        let thingies = [env.ENSURE_THINGY.split(':')[0]]
        const stats = {
          total: 0,
          hits: 0,
          misses: 0,
          results: 0,
          modified: 0,
          unmodified: 0
        }
        client.on('cache-hit', () => stats.hits++)
        client.on('cache-miss', () => stats.misses++)
        client.on('cache-results', () => stats.results++)
        client.on('cache-unmodified', () => stats.unmodified++)
        client.on('cache-modified', () => stats.modified++)
        const updateWidgets = function (callback) {
          debug(`Updating list of widgets; currently have ${widgets}`)
          return client.get('/widget', (err, results) => {
            if (err) {
              return callback(err)
            } else {
              stats.total++
              debug(`Got ${results.length} results`)
              widgets = _.map(results, 'id')
              debug(`New widgets ${widgets}`)
              return callback(null)
            }
          })
        }
        const updateThingies = function (callback) {
          debug(`Updating list of thingies; currently have ${thingies}`)
          return client.get('/thingy', (err, results) => {
            if (err) {
              return callback(err)
            } else {
              stats.total++
              debug(`Got ${results.length} results`)
              thingies = _.map(results, 'id')
              debug(`New thingies ${thingies}`)
              return callback(null)
            }
          })
        }
        const addWidget = function (callback) {
          const props = {
            name: `Widget at ${Date.now()}`,
            size: _.random(1, 10)
          }
          return client.post('/widget', props, (err, results) => {
            if (err) {
              return callback(err)
            } else {
              widgets = _.union(widgets, [results.id])
              return callback(null)
            }
          })
        }
        const addThingy = function (callback) {
          debug('Adding a new thingy')
          const props =
            {name: `Thingy at ${Date.now()}`}
          return client.post('/thingy', props, (err, results) => {
            if (err) {
              return callback(err)
            } else {
              debug(`Created thingy ${results.id}; adding to list`)
              thingies = _.union(thingies, [results.id])
              debug(`Now thingies is ${thingies}`)
              return callback(null)
            }
          })
        }
        const putWidget = function (callback) {
          const id = _.sample(widgets)
          return client.get(`/widget/${id}`, (err, results) => {
            if (err) {
              return callback(err)
            } else {
              stats.total++
              _.assign(results, {
                name: `Widget at ${Date.now()}`,
                size: _.random(1, 10)
              }
              )
              return client.put(`/widget/${id}`, results, (err, putted) => {
                if (err) {
                  return callback(err)
                } else {
                  return callback(null)
                }
              })
            }
          })
        }
        const patchWidget = function (callback) {
          const id = _.sample(widgets)
          const props = {}
          if (_.random(0, 1) === 0) {
            props.name = `Widget at ${Date.now()}`
          } else {
            props.size = _.random(1, 10)
          }
          return client.patch(`/widget/${id}`, props, (err, results) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null)
            }
          })
        }
        const deleteWidget = function (callback) {
          if (widgets.length <= 1) {
            return callback(null)
          } else {
            const id = _.sample(widgets)
            return client.delete(`/widget/${id}`, (err, results) => {
              if (err) {
                return callback(err)
              } else {
                widgets = _.difference(widgets, [id])
                return callback(null)
              }
            })
          }
        }
        const getWidget = function (callback) {
          const id = _.sample(widgets)
          debug(`Getting widget ${id}`)
          return client.get(`/widget/${id}`, (err, results) => {
            if (err) {
              debug(`Error getting widget ${id}: ${err}`)
              return callback(err)
            } else {
              stats.total++
              debug(`Success getting widget ${id}`)
              debug(results)
              return callback(null)
            }
          })
        }
        const getThingy = function (callback) {
          const id = _.sample(thingies)
          debug(`Getting thingy ${id}`)
          return client.get(`/thingy/${id}`, (err, results) => {
            if (err) {
              debug(`Error getting thingy ${id}: ${err}`)
              return callback(err)
            } else {
              stats.total++
              debug(`Got thingy ${id}`)
              debug(results)
              return callback(null)
            }
          })
        }
        const tasks = [
          updateWidgets,
          updateThingies,
          addWidget,
          addThingy,
          putWidget,
          patchWidget,
          deleteWidget,
          getWidget,
          getThingy
        ]
        const runTask = function (i, callback) {
          if (widgets.length === 0) {
            return addWidget(callback)
          } else {
            const task = _.sample(tasks)
            return task(callback)
          }
        }

        async.times(2000, runTask, err => {
          return setImmediate(() => {
            if (err) {
              return this.callback(err)
            } else {
              return this.callback(null, stats)
            }
          })
        })
        return undefined
      },

      'it works' (err, stats) {
        assert.ifError(err)
        assert.equal(stats.total, (stats.hits + stats.misses),
          `Total != hits + misses ${JSON.stringify(stats)}`)
        const expected = (stats.results + stats.modified + stats.unmodified)
        assert.equal(stats.hits, expected,
          `Hits != results + modified + unmodified ${JSON.stringify(stats)}`)
      }
    }
  })).export(module)
