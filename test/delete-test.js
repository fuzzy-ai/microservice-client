/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// delete-test.coffee
// Test the delete() method of microservice-client
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const microserviceClientBatch = require('./batch')

vows
  .describe('delete() method')
  .addBatch(microserviceClientBatch({
    'and we DELETE a resource': {
      topic (client) {
        const { callback } = this
        client.delete('/widget/c729d597-eabb-41d7-a2c7-edc8766f89be', callback)
        return undefined
      },
      'it works' (err) {
        assert.ifError(err)
      }
    }
  })).export(module)
