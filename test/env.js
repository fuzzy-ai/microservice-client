// env.coffee
// Environment variables for tests
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

module.exports = {
  PORT: '1516',
  HOSTNAME: 'localhost',
  APP_KEY_UNITTEST: 'raptcarrvisaparrpar',
  LOG_FILE: '/dev/null',
  DRIVER: 'memory',
  PARAMS: '{}',
  ENSURE_WIDGET: 'c729d597-eabb-41d7-a2c7-edc8766f89be:ensured:5',
  ENSURE_THINGY: 'f89b2a83-5f8a-4288-9dbc-09a2c693c04e:a thingy'
}
