/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// get-delayed-test.coffee
// Test the get() method on an URL that is delayed past our max wait
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const env = require('./env')
const microserviceClientBatch = require('./batch')

const options = {
  root: `http://${env.HOSTNAME}:${env.PORT}/`,
  key: env.APP_KEY_UNITTEST,
  maxWait: 3
}

vows
  .describe('get() method on delayed')
  .addBatch(microserviceClientBatch(options, {
    'and we GET a resource that takes too long': {
      topic (client) {
        const { callback } = this
        client.get('/delay?count=5', (err, body) => {
          if (err) {
            if (err.name === 'TimeoutError') {
              return callback(null)
            } else {
              return callback(err)
            }
          } else {
            return callback(new Error('Unexpected success'))
          }
        })
        return undefined
      },
      'it fails correctly' (err) {
        assert.ifError(err)
      }
    }
  }
  )).export(module)
