/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// get-test.coffee
// Test the get() method of microservice-client
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const microserviceClientBatch = require('./batch')

vows
  .describe('get() method')
  .addBatch(microserviceClientBatch({
    'and we GET a resource': {
      topic (client) {
        const { callback } = this
        client.get('/widget/c729d597-eabb-41d7-a2c7-edc8766f89be', callback)
        return undefined
      },
      'it works' (err, widget) {
        assert.ifError(err)
        assert.isObject(widget)
        assert.equal('c729d597-eabb-41d7-a2c7-edc8766f89be', widget.id)
        assert.equal('ensured', widget.name)
        assert.equal('5', widget.size)
        assert.isString(widget.createdAt)
        assert.isString(widget.updatedAt)
      }
    }
  })).export(module)
