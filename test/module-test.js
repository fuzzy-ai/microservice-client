/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// module-test.coffee
// Test the basic parameters for the microservice client
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const API_KEY = 'raptcarrvisaparrpar'

vows
  .describe('MicroserviceClient module')
  .addBatch({
    'When we load the MicroserviceClient module': {
      topic () {
        const { callback } = this
        try {
          const MicroserviceClient = require('../lib/microservice-client')
          callback(null, MicroserviceClient)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, MicroserviceClient) {
        assert.ifError(err)
        assert.isFunction(MicroserviceClient)
      },
      'and we create an instance': {
        topic (MicroserviceClient) {
          const { callback } = this
          try {
            const client = new MicroserviceClient('http://localhost:1516/', API_KEY)
            callback(null, client)
          } catch (err) {
            callback(err, null)
          }
          return undefined
        },
        'it works' (err, client) {
          assert.ifError(err)
          assert.isObject(client)
        },
        'and we examine its methods': {
          topic (client) {
            return client
          },
          'it has the get() method' (err, client) {
            assert.ifError(err)
            assert.isFunction(client.get)
          },
          'it has the post() method' (err, client) {
            assert.ifError(err)
            assert.isFunction(client.post)
          },
          'it has the put() method' (err, client) {
            assert.ifError(err)
            assert.isFunction(client.put)
          },
          'it has the patch() method' (err, client) {
            assert.ifError(err)
            assert.isFunction(client.patch)
          },
          'it has the delete() method' (err, client) {
            assert.ifError(err)
            assert.isFunction(client.delete)
          }
        }
      }
    }}).export(module)
