// no-default-key-test.js
// Test using no default OAuth 2.0 key

// Copyright 2018 Fuzzy.ai <legal@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const TestServer = require('./testserver')
const env = require('./env')

const ROOT = `http://${env.HOSTNAME}:${env.PORT}/`

vows.describe('No default key')
  .addBatch({
    'When we start a test microservice': {
      topic () {
        const { callback } = this
        try {
          const server = new TestServer(env)
          server.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, server)
            }
          })
        } catch (error) {
          const err = error
          callback(err)
        }
        return undefined
      },
      'it works' (err, server) {
        assert.ifError(err)
        assert.isObject(server)
      },
      teardown (server) {
        const { callback } = this
        if ((server != null) && server.stop) {
          server.stop(err => callback(err))
        } else {
          callback(null)
        }
        return undefined
      },
      'and we create a new client with positional params and no default key': {
        topic () {
          const MicroserviceClient = require('../lib/microservice-client')
          const client = new MicroserviceClient(ROOT)
          return client
        },
        'it works': (err, client) => {
          assert.ifError(err)
          assert.isObject(client)
        },
        'and we get a resource that does not require authentication': {
          topic (client) {
            client.get('/no-auth-required', this.callback)
            return undefined
          },
          'it works': (err, body) => {
            assert.ifError(err)
            assert.isObject(body)
            assert.equal(body.message, "That's fine.")
          }
        }
      },
      'and we create a new client with named params and no key': {
        topic () {
          const MicroserviceClient = require('../lib/microservice-client')
          const client = new MicroserviceClient({root: ROOT})
          return client
        },
        'it works': (err, client) => {
          assert.ifError(err)
          assert.isObject(client)
        },
        'and we get a resource that does not require authentication': {
          topic (client) {
            client.get('/no-auth-required', this.callback)
            return undefined
          },
          'it works': (err, body) => {
            assert.ifError(err)
            assert.isObject(body)
            assert.equal(body.message, "That's fine.")
          }
        }
      }
    }
  })
  .export(module)
