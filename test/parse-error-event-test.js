/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// error-event-test.coffee
// Test the get() method of microservice-client
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const async = require('async')
const assert = vows.assert

const microserviceClientBatch = require('./batch')

vows
  .describe('Test emission of the "error" event for bad JSON')
  .addBatch(microserviceClientBatch({
    'and we test the client for event-emitter methods': {
      topic (client) {
        return client
      },
      'it has the "on" method' (err, client) {
        assert.ifError(err)
        assert.isFunction(client.on)
      },
      'and we GET badly-formatted JSON': {
        topic (client) {
          const { callback } = this
          async.parallel([
            callback =>
              client.on('error', err => callback(null)), // eslint-disable-line handle-callback-err
            callback =>
              client.get('/bad-json', (err, data) => {
                if (err && (err.name === 'SyntaxError')) {
                  return callback(null)
                } else if (err) {
                  return callback(err)
                } else {
                  return callback(new Error('Unexpected success'))
                }
              })

          ], err => callback(err))
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        }
      }
    }
  })).export(module)
