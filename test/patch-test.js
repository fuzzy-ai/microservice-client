/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// patch-test.coffee
// Test the patch() method of the microservice client
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const microserviceClientBatch = require('./batch')

vows
  .describe('patch() method')
  .addBatch(microserviceClientBatch({
    'and we PATCH a resource': {
      topic (client) {
        const { callback } = this
        const props =
          {name: 'patched'}
        client.patch('/widget/c729d597-eabb-41d7-a2c7-edc8766f89be', props, callback)
        return undefined
      },
      'it works' (err, widget) {
        assert.ifError(err)
        assert.isObject(widget)
        assert.isString(widget.id)
        assert.isString(widget.createdAt)
        assert.isString(widget.updatedAt)
        assert.equal('patched', widget.name)
        assert.equal('5', widget.size)
      }
    }
  })).export(module)
