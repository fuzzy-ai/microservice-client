/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// post-test.coffee
// Test the post() method
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const util = require('util')

const vows = require('vows')
const assert = vows.assert
const debug = require('debug')('microservice-client-post-test')

const microserviceClientBatch = require('./batch')

vows
  .describe('post() method')
  .addBatch(microserviceClientBatch({
    'and we POST a resource': {
      topic (client) {
        const { callback } = this
        const props = {
          name: 'posted',
          size: '4'
        }
        debug(util.inspect(props))
        client.post('/widget', props, callback)
        return undefined
      },
      'it works' (err, widget) {
        assert.ifError(err)
        assert.isObject(widget)
        assert.isString(widget.id)
        assert.isString(widget.createdAt)
        assert.isString(widget.updatedAt)
        assert.equal('posted', widget.name)
        assert.equal('4', widget.size)
      }
    }
  })).export(module)
