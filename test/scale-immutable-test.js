/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// scale-immutable-test.coffee
// Test scaling with an immmutable endpoint
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const async = require('async')
const vows = require('vows')
const assert = vows.assert

const microserviceClientBatch = require('./batch')

process.on('uncaughtException', (err) => {
  console.error(err)
  return process.exit(-1)
})

vows
  .describe('Scale on immutable resources')
  .addBatch(microserviceClientBatch({
    'and we repeatedly GET a resource': {
      topic (client) {
        const getImmutable = function (i, callback) {
          const url = '/thingy/f89b2a83-5f8a-4288-9dbc-09a2c693c04e'
          return client.get(url, (err, body) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null)
            }
          })
        }
        async.times(100, getImmutable, err => {
          return this.callback(err)
        })
        return undefined
      },
      'it works' (err) {
        assert.ifError(err)
      }
    }
  })).export(module)
