/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// error-event-test.coffee
// Test the get() method of microservice-client
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert
const async = require('async')

const microserviceClientBatch = require('./batch')

vows
  .describe('Test emission of the "error" event')
  .addBatch(microserviceClientBatch({
    'and we test the client for event-emitter methods': {
      topic (client) {
        return client
      },
      'it has the "on" method' (err, client) {
        assert.ifError(err)
        assert.isFunction(client.on)
      },
      'and we GET a flaky resource': {
        topic (client) {
          const { callback } = this
          async.parallel([
            callback =>
              client.on('error', err => callback(null, err)),
            callback => client.get('/flaky', callback)
          ], (err, results) => callback(err, results[0]))
          return undefined
        },
        'it works' (err, obj) {
          assert.ifError(err)
        },
        'its error looks correct' (err, obj) {
          assert.ifError(err)
          assert.isObject(obj)
          assert.isString(obj.name)
          assert.equal(obj.name, 'ServerError')
          assert.isNumber(obj.statusCode)
          assert.equal(obj.statusCode, 500)
          assert.isObject(obj.headers)
          for (const name in obj.headers) {
            const value = obj.headers[name]
            assert.isString(name)
            assert.isString(value)
          }
          assert.isString(obj.url)
          assert.equal(obj.url, 'http://localhost:1516/flaky')
          assert.isString(obj.verb)
          assert.equal(obj.verb, 'GET')
          assert.isString(obj.body)
          assert.isString(obj.message)
          assert.equal(obj.message, 'GET on http://localhost:1516/flaky resulted in 500 Internal Server Error')
        }
      }
    }
  })).export(module)
