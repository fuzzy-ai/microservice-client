/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// server-unavailable-test.coffee
// Test handling of connections to servers that are unavailable
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert
const async = require('async')

const microserviceClientBatch = require('./batch')

process.on('uncaughtException', (err) => {
  console.error(err)
  return process.exit(-1)
})

vows
  .describe('Server unavailable')
  .addBatch(microserviceClientBatch({
    'and we GET a resource from a server that is just coming online': {
      topic (client, server) {
        async.waterfall([
          callback =>
            // First, stop the server
            server.stop(callback),
          callback =>
            // Then in parallel...
            async.parallel([
              callback =>
                // Try to get a resource from the server
                client.get('/widget/c729d597-eabb-41d7-a2c7-edc8766f89be', callback),
              function (callback) {
                // Wait 5 seconds and restart the server
                const restartServer = () => server.start(callback)
                return setTimeout(restartServer, 5000)
              }
            ], callback)

        ], err => {
          return this.callback(err)
        })
        return undefined
      },
      'it eventually works' (err) {
        assert.ifError(err)
      }
    }
  })).export(module)
