/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// stop-test.coffee
// Test for stopping the client

// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const env = require('./env')
const TestServer = require('./testserver')

const ROOT = `http://${env.HOSTNAME}:${env.PORT}/`
const KEY = env.APP_KEY_UNITTEST
const TIMEOUT = 60000

vows.describe('stop() method')
  .addBatch({
    'When we start a test microservice': {
      topic () {
        const { callback } = this
        try {
          const server = new TestServer(env)
          server.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, server)
            }
          })
        } catch (error) {
          const err = error
          callback(err)
        }
        return undefined
      },
      'it works' (err, server) {
        assert.ifError(err)
        assert.isObject(server)
      },
      teardown (server) {
        const { callback } = this
        if ((server != null) && server.stop) {
          server.stop(err => callback(err))
        } else {
          callback(null)
        }
        return undefined
      },
      'and we create a microservice client with a long timeout': {
        topic () {
          const { callback } = this
          try {
            const MicroserviceClient = require('../lib/microservice-client')
            const client = new MicroserviceClient({
              root: ROOT,
              key: KEY,
              timeout: TIMEOUT
            })
            callback(null, client)
          } catch (err) {
            callback(err, null)
          }
          return undefined
        },
        'it works' (err, client) {
          assert.ifError(err)
          assert.isObject(client)
        },
        'and we GET a resource': {
          topic (client) {
            client.get('/widget/c729d597-eabb-41d7-a2c7-edc8766f89be', err => {
              return this.callback(err)
            })
            return undefined
          },
          'it works' (err) {
            assert.ifError(err)
          },
          'and we stop the client': {
            topic (client) {
              client.stop(this.callback)
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            }
          }
        }
      }
    }}).export(module)
