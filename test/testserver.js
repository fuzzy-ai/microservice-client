/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// testserver.coffee
// Test class implementing the server side
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const util = require('util')

const _ = require('lodash')
const Microservice = require('@fuzzy-ai/microservice')
const uuid = require('uuid')
const {DatabankObject} = require('databank')
const debug = require('debug')('microservice-client:testserver')
const async = require('async')

const Widget = DatabankObject.subClass('Widget')
Widget.schema = {
  pkey: 'id',
  fields: [
    'name',
    'size',
    'createdAt',
    'updatedAt'
  ]
}

Widget.beforeCreate = function (props, callback) {
  if ((props.id == null)) {
    props.id = uuid.v1()
  }
  props.createdAt = (props.updatedAt = (new Date()).toISOString())
  return callback(null, props)
}

Widget.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = (new Date()).toISOString()
  return callback(null, props)
}

Widget.prototype.beforeSave = function (callback) {
  if ((this.id == null)) {
    this.id = uuid.v1()
  }

  if ((this.createdAt == null)) {
    this.createdAt = (new Date()).toISOString()
  }

  this.updatedAt = (new Date()).toISOString()
  return callback(null)
}

const Thingy = DatabankObject.subClass('Thingy')
Thingy.schema = {
  pkey: 'id',
  fields: [
    'name',
    'createdAt',
    'updatedAt'
  ]
}

Thingy.beforeCreate = function (props, callback) {
  if ((props.id == null)) {
    props.id = uuid.v1()
  }
  props.createdAt = (props.updatedAt = (new Date()).toISOString())
  return callback(null, props)
}

Thingy.prototype.beforeUpdate = (props, callback) => callback(new Error('immutable'))

Thingy.prototype.beforeSave = callback => callback(new Error('immutable'))

Thingy.prototype.beforeDel = callback => callback(new Error('immutable'))

class TestServer extends Microservice {
  getName () { return 'test' }

  environmentToConfig (env) {
    const config = super.environmentToConfig(env)
    config.ensureWidget = env.ENSURE_WIDGET
    config.ensureThingy = env.ENSURE_THINGY
    return config
  }

  setupMiddleware (exp) {
    return exp.use((req, res, next) => {
      debug(`${req.method} ${req.url}`)
      debug(req.headers)
      return next()
    })
  }

  setupParams (exp) {
    exp.param('wid', (req, res, next, id) => {
      debug(`Looking up widget ${id}`)
      return Widget.get(id, (err, widget) => {
        if (err) {
          debug(`Error looking up widget ${id}: ${err}`)
          return next(err)
        } else {
          debug(`Success looking up widget ${id}`)
          debug(widget)
          req.widget = widget
          return next()
        }
      })
    })

    exp.param('tid', (req, res, next, id) => {
      debug(`Getting thingy ${id}`)
      return Thingy.get(id, (err, thingy) => {
        if (err) {
          debug(`Error getting thingy ${id}: ${err}`)
          return next(err)
        } else {
          debug(`Success getting thingy ${id}`)
          req.thingy = thingy
          return next()
        }
      })
    })

    return exp
  }

  setupRoutes (exp) {
    exp.get('/widget', this.appAuthc, (req, res, next) => {
      debug('Getting all widgets')
      const widgets = []
      const accum = widget => widgets.push(widget)
      return Widget.scan(accum, (err) => {
        if (err) {
          return next(err)
        } else {
          debug(`Got all widgets (${widgets.length})`)
          debug(widgets)
          return res.json(widgets)
        }
      })
    })

    exp.post('/widget', this.appAuthc, (req, res, next) => {
      const props = _.omit(req.body, 'createdAt', 'updatedAt')
      debug('Creating new widget')
      debug(util.inspect(props))
      return Widget.create(props, (err, widget) => {
        if (err) {
          return next(err)
        } else {
          debug(`Created widget ${widget.id}`)
          debug(widget)
          return res.json(widget)
        }
      })
    })

    exp.get('/widget/:wid', this.appAuthc, (req, res, next) => {
      debug(`Getting widget ${req.widget.id}`)
      res.set('Last-Modified', (new Date(req.widget.updatedAt)).toUTCString())
      return res.json(req.widget)
    })

    exp.put('/widget/:wid', this.appAuthc, (req, res, next) => {
      const props = _.omit(req.body, 'id', 'createdAt', 'updatedAt')
      props.id = req.widget.id
      props.createdAt = req.widget.createdAt
      debug(`Overwriting widget ${req.widget.id}`)
      debug(util.inspect(props))
      const widget = new Widget(props)
      return widget.save((err, saved) => {
        if (err) {
          return next(err)
        } else {
          debug(`Saved widget ${req.widget.id}`)
          debug(util.inspect(saved))
          return res.json(saved)
        }
      })
    })

    exp.patch('/widget/:wid', this.appAuthc, (req, res, next) => {
      const props = _.omit(req.body, 'id', 'createdAt', 'updatedAt')
      debug(`Patching widget ${req.widget.id}`)
      debug(util.inspect(props))
      Object.assign(req.widget, props)
      return req.widget.save((err, saved) => {
        if (err) {
          return next(err)
        } else {
          debug(`Saved widget ${req.widget.id}`)
          debug(saved)
          return res.json(saved)
        }
      })
    })

    exp.delete('/widget/:wid', this.appAuthc, (req, res, next) => {
      debug(`Deleting widget ${req.widget.id}`)
      return req.widget.del((err) => {
        if (err) {
          return next(err)
        } else {
          debug(`Deleted widget ${req.widget.id}`)
          return res.status(204).send()
        }
      })
    })

    let counter = 0

    exp.get('/flaky', this.appAuthc, (req, res, next) => {
      debug('Getting flaky resource')
      counter++
      if ((counter % 2) === 0) {
        debug('Success getting /flaky')
        return res.status(200).json({status: 'OK'})
      } else {
        debug('Failure getting /flaky')
        return res.status(500).json({status: 'Error'})
      }
    })

    exp.get('/bad-json', this.appAuthc, (req, res, next) => {
      debug('Returning malformed JSON')
      res.set('Content-type', 'application/json; charset=utf-8')
      return res.end('{"bad-json": true')
    })

    exp.get('/plain-text', this.appAuthc, (req, res, next) => {
      debug('Returning plain text (not JSON)')
      res.set('Content-type', 'text/plain')
      return res.end('Hello, world!')
    })

    exp.get('/some-html5', this.appAuthc, (req, res, next) => {
      debug('Returning html5')
      res.set('Content-type', 'text/html')
      return res.end(`\
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Some HTML5</title>
  </head>
  <body>
    Enjoy.
  </body>
</html>\
`
      )
    })

    exp.get('/thingy', this.appAuthc, (req, res, next) => {
      debug('Getting all thingies')
      const thingies = []
      const accum = thingy => thingies.push(thingy)
      return Thingy.scan(accum, (err) => {
        if (err) {
          debug(`Error getting thingies: ${err}`)
          return next(err)
        } else {
          debug(`Got all thingies (${thingies.length})`)
          debug(thingies)
          return res.json(thingies)
        }
      })
    })

    exp.post('/thingy', this.appAuthc, (req, res, next) => {
      const props = _.omit(req.body, 'createdAt', 'updatedAt')
      debug('Creating new thingy')
      debug(props)
      return Thingy.create(props, (err, thingy) => {
        if (err) {
          debug(`Error creating thingy: ${err}`)
          return next(err)
        } else {
          debug(`Created thingy ${thingy.id}`)
          debug(thingy)
          return res.json(thingy)
        }
      })
    })

    exp.get('/thingy/:tid', this.appAuthc, (req, res, next) => {
      debug(`Getting thingy ${req.thingy.id}`)
      res.set('Last-Modified', (new Date(req.thingy.updatedAt)).toUTCString())
      const lm = Date.parse(req.thingy.updatedAt)
      const expires = lm + (10 * 365 * 24 * 60 * 60 * 1000)
      res.set('Expires', (new Date(expires)).toUTCString())
      return res.json(req.thingy)
    })

    exp.get('/delay', this.appAuthc, (req, res, next) => {
      const count = req.query.count || 1
      debug(`Delaying for count of ${count}`)
      const start = Date.now()
      const finish = function () {
        const end = Date.now()
        return res.json({total: end - start})
      }
      return setTimeout(finish, count * 1000)
    })

    exp.get('/no-auth-required', (req, res, next) => {
      if (_.isString(req.headers.authorization)) {
        res.status(400).end('No authorization allowed')
      } else {
        res.json({message: "That's fine."})
      }
    })
  }

  getSchema () {
    return {
      Widget: Widget.schema,
      Thingy: Thingy.schema
    }
  }

  startCustom (callback) {
    return async.waterfall([
      callback => {
        if (!this.config.ensureWidget) {
          debug('No widget to ensure')
          return callback(null)
        } else {
          const [id, name, size] = Array.from(this.config.ensureWidget.split(':'))
          debug(`Ensuring widget ${id} (name = ${name}, size = ${size})`)
          return Widget.get(id, (err, widget) => {
            if (err && (err.name === 'NoSuchThingError')) {
              debug(`Widget ${id} did not exist; creating it`)
              return Widget.create({id, name, size}, (err, widget) => {
                if (err) {
                  debug(`Error creating widget ${id}: ${err}`)
                  return callback(err)
                } else {
                  debug(`Success creating widget ${id}`)
                  return callback(null)
                }
              })
            } else if (err) {
              debug(`Error getting widget ${id} during ensure: ${err}`)
              return callback(err)
            } else {
              debug(`Widget ${id} already exists`)
              return callback(null)
            }
          })
        }
      },
      callback => {
        if (!this.config.ensureThingy) {
          debug('No thingy to ensure')
          return callback(null)
        } else {
          const [id, name] = Array.from(this.config.ensureThingy.split(':'))
          debug(`Ensuring thingy ${id} (name = ${name})`)
          return Thingy.get(id, (err, thingy) => {
            if (err && (err.name === 'NoSuchThingError')) {
              debug(`Thingy ${id} does not exist; creating it`)
              return Thingy.create({id, name}, (err, thingy) => {
                if (err) {
                  debug(`Error creating thingy ${id}: ${err}`)
                  return callback(err)
                } else {
                  debug(`Success creating thingy ${id}`)
                  return callback(null)
                }
              })
            } else if (err) {
              debug(`Error getting thingy ${id}: ${err}`)
              return callback(err)
            } else {
              debug(`Thingy ${id} already exists`)
              return callback(null)
            }
          })
        }
      }
    ], callback)
  }
}

module.exports = TestServer
