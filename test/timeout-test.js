// timeout-test.coffee
// Test servers that take longer than our max timeout value to respond
//
// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert
const async = require('async')
const debug = require('debug')('microservice-client:timeout-test')

const env = require('./env')
const TestServer = require('./testserver')

const ROOT = `http://${env.HOSTNAME}:${env.PORT}/`
const KEY = env.APP_KEY_UNITTEST

process.on('uncaughtException', (err) => {
  console.log('uncaughtException')
  console.log(err.stack)
  console.error(err)
  return process.exit(-1)
})

vows
  .describe('Timeout test')
  .addBatch({
    'When we create a test microservice but do not start it': {
      topic () {
        try {
          const server = new TestServer(env)
          this.callback(null, server)
        } catch (err) {
          this.callback(err)
        }
        return undefined
      },
      'it works' (err, server) {
        assert.ifError(err)
        assert.isObject(server)
      },
      'and we create a microservice client with maxWait = 3': {
        topic (server) {
          const { callback } = this
          try {
            const MicroserviceClient = require('../lib/microservice-client')
            const client = new MicroserviceClient({
              root: ROOT,
              key: KEY,
              maxWait: 2
            })
            callback(null, client)
          } catch (err) {
            callback(err, null)
          }
          return undefined
        },
        'it works' (err, client) {
          assert.ifError(err)
          assert.isObject(client)
        },
        'and we GET a resource from a server that is down past our wait time': {
          topic (client, server) {
            async.parallel([
              function (callback) {
                // Try to get a resource from the server
                const url = '/widget/c729d597-eabb-41d7-a2c7-edc8766f89be'
                debug(`Getting ${url}`)
                return client.get(url, (err) => {
                  debug('client complete')
                  if (err) {
                    debug(`Got an error ${err}`)
                    if (err.name === 'TimeoutError') {
                      debug(err.message)
                      debug("It's a timeout error; great")
                      return callback(null)
                    } else {
                      debug("It's another kind of error")
                      return callback(err)
                    }
                  } else {
                    debug("No error; that's wrong.")
                    return callback(new Error('Unexpected success'))
                  }
                })
              },
              callback =>
                async.waterfall([
                  function (callback) {
                    // Wait 5 seconds and restart the server
                    const startServer = function () {
                      debug('Starting server')
                      return server.start(callback)
                    }
                    debug('Waiting 3s to start server')
                    return setTimeout(startServer, 3000)
                  },
                  function (callback) {
                    const stopServer = function () {
                      debug('Stopping server')
                      return server.stop(callback)
                    }
                    debug('Waiting 1s to stop server')
                    return setTimeout(stopServer, 1000)
                  }
                ], callback)

            ], err => {
              return this.callback(err)
            })
            return undefined
          },
          'it fails correctly' (err) {
            assert.ifError(err)
          }
        }
      }
    }}).export(module)
