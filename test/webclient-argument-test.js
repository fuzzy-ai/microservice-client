/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// webclient-argument-test.coffee
// Test for stopping the client

// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = vows.assert

const web = require('@fuzzy-ai/web')

const env = require('./env')
const microserviceClientBatch = require('./batch')

const options = {
  root: `http://${env.HOSTNAME}:${env.PORT}/`,
  key: env.APP_KEY_UNITTEST,
  webClient: new web.WebClient({timeout: 60000})
}

vows.describe('webClient argument')
  .addBatch(microserviceClientBatch(options, {
    'and we GET a resource': {
      topic (client) {
        client.get('/widget/c729d597-eabb-41d7-a2c7-edc8766f89be', err => {
          return this.callback(err)
        })
        return undefined
      },
      'it works' (err) {
        assert.ifError(err)
      },
      'and we stop the client': {
        topic (client) {
          client.stop(this.callback)
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        }
      }
    }
  }
  )).export(module)
